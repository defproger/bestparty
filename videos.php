<!DOCTYPE html>
<html lang="ru">
<head>
    <link rel="stylesheet" href="css/style.css">

    <link rel="shortcut icon" href="img/favicon/favi.ico">
    <!--Words-->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Montserrat+Alternates|Open+Sans|Oswald&amp;subset=cyrillic" rel="stylesheet">
    <meta charset="UTF-8">
    <script src="./js/jquery.js" charset="utf-8"></script>
    <script src="./js/jquery-ui.js" charset="utf-8"></script>
    <script src="./js/animation.js" charset="utf-8"></script>
    <title>Best Party</title>
</head>

<body>
<!--  кфмеквапрослюолорысмитлавцкыс ры6кяевкачсру5вкану5вканп -->
<section class="layer  modificator">
    <div class="">
        <div class="titlemid">
            <h3 class="sectionsubtitle">Живие примеры наших работ</h3>
            <h2 class="sectiontitle">Видео недавних праздников</h2>
            <div class="sectiontext">
                <p>
                    <!-- Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril ceteros lucilius, nec dolor ullamcorper ut. integre ad est, sonet inciderint signiferumque ne nec. Id eos consul salutatus iracundia, nobis patrioque id usu. Vis ei alii putant evertitur, pri eius verear voluptatibus in, no suas deterruisset -->
                </p>
            </div>
        </div>
        <section class="videoscreen" id="video">
            <div class="video">
                <?php
                $dir = opendir("./video/");
                $count = 0;

                while ($file = readdir($dir)) {
                    if ($file == '.' || $file == '..' || is_dir('./video' . $file)) {
                        continue;
                    }
                    $count++;
                }

                    for ($i = 1; $i < $count; $i++) {
                        echo file_get_contents('./video/'.$i.'.txt');
                    }



                ?>
            </div>
        </section>
    </div>
    <!--  кфмеквапрослюолорысмитлавцкыс ры6кяевкачсру5вкану5вканп -->
</body>
</html>
