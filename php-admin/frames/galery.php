<?php
$name_path = $_POST['name_path'];
$komment = $_POST['komment'];
$date_name = $_POST['date_name'];

$uploads_dir = '../../img/works';
$fd = fopen($uploads_dir.'/'.$name_path.".txt", 'w') or die("не удалось создать файл");
$fd_for_date = fopen($uploads_dir.'/'.$name_path."date.txt", 'w') or die("не удалось создать файл");

if(isset($_FILES) && $_FILES['inputfile']['error'] == 0){

file_put_contents($uploads_dir.'/'.$name_path.'.txt', '');
file_put_contents($uploads_dir.'/'.$name_path.'date.txt', '');

fwrite($fd, $komment);
fclose($fd);

fwrite($fd_for_date, $date_name);
fclose($fd_for_date);

$destiation_dir = $uploads_dir.'/';
move_uploaded_file($_FILES['inputfile']['tmp_name'], $destiation_dir.$name_path.'.jpg' );
echo 'File Uploaded';
}else{
echo 'No File Uploaded';
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<link rel="stylesheet" href="../../css/style.css">

	<link rel="shortcut icon" href="../../img/favicon/favi.ico">
	<!--Words-->
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC|Montserrat+Alternates|Open+Sans|Oswald&amp;subset=cyrillic" rel="stylesheet">
	<meta charset="UTF-8">
	<script src="../../js/jquery.js" charset="utf-8"></script>
	<script src="../../js/jquery-ui.js" charset="utf-8"></script>
	<script src="../../js/animation.js" charset="utf-8"></script>
	<title>Best Party</title>
</head>
  <body>
    <center>
      <form method="post" action="galery.php" enctype="multipart/form-data">
      <input type="file" id="inputfile" name="inputfile"></br /><br />
      <input type="text" name="name_path" placeholder="номер"></br /><br />
      <input type="text" name="komment" placeholder="подпись"></br /><br />
      <input type="text" name="date_name" placeholder="дата"></br /><br />
      <input type="submit" value="Загрузить">
    </center>

		<div class="otstup-php"></div>
		<div class="works">
				<div class="workscolum">
					<div class="worksitem">
						<img class="workimg" src="../../img/works/1.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_1 = '../../img/works/1.txt';
							$works_date_1 = '../../img/works/1date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_1); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_1); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/2.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_2 = '../../img/works/2.txt';
							$works_date_2 = '../../img/works/2date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_2); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_2); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/3.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_3 = '../../img/works/3.txt';
							$works_date_3 = '../../img/works/3date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_3); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_3); ?></div>
						</div>
					</div>

				</div>
				<div class="workscolum">
					<div class="worksitem">
						<img class="workimg" src="../../img/works/4.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_4 = '../../img/works/4.txt';
							$works_date_4 = '../../img/works/4date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_4); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_4); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/5.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_5 = '../../img/works/5.txt';
							$works_date_5 = '../../img/works/5date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_5); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_5); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/6.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_6 = '../../img/works/6.txt';
							$works_date_6 = '../../img/works/6date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_6); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_6); ?></div>
						</div>
					</div>
				</div>
				<div class="workscolum">
					<div class="worksitem" id="workimg">
						<img class="workimg" src="../../img/works/7.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_7 = '../../img/works/7.txt';
							$works_date_7 = '../../img/works/7date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_7); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_7); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/8.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_8 = '../../img/works/8.txt';
							$works_date_8 = '../../img/works/8date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_8); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_8); ?></div>
						</div>
					</div>
				</div>
				<div class="workscolum">
					<div class="worksitem">
						<img class="workimg" src="../../img/works/9.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_9 = '../../img/works/9.txt';
							$works_date_9 = '../../img/works/9date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_9); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_9); ?></div>
						</div>


					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/10.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_10 = '../../img/works/10.txt';
							$works_date_10 = '../../img/works/10date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_10); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_10); ?></div>
						</div>
					</div>
					<div class="worksitem">
						<img class="workimg" src="../../img/works/11.jpg" alt="">
						<div class="workshover">
							<img class="workimglogo" src="../../img/works/gallery.png" alt="">
							<?php
							$works_img_11 = '../../img/works/11.txt';
							$works_date_11 = '../../img/works/11date.txt';
							?>
							<div class="worktitle"><?php echo file_get_contents($works_img_11); ?></div>
							<div class="worksubtitle"><?php echo file_get_contents($works_date_11); ?></div>
						</div>
					</div>

				</div>
			</div>
  </body>
</html>
