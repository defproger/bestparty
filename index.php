<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);
?>

<!DOCTYPE html>
<html lang="ru">
<head>

    <link rel="stylesheet" href="css/style.css">

    <link rel="shortcut icon" href="img/favicon/favi.ico">
    <!--Words-->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Montserrat+Alternates|Open+Sans|Oswald&amp;subset=cyrillic"
          rel="stylesheet">
    <meta charset="UTF-8">
    <script src="./js/jquery.js" charset="utf-8"></script>
    <script src="./js/jquery-ui.js" charset="utf-8"></script>
    <script src="./js/animation.js" charset="utf-8"></script>
    <script src="./js/preloader.js" charset="utf-8"></script>
    <title>Best Party</title>
</head>

<body>
<div class="preloader">
    <div class="loader">
        <video autoplay muted class="bgvideo" id="bgvideo" src="img/bp.mp4"></video>
    </div>
</div>


<header>
    <div class="container">

        <div class="headerin">
            <div class="logohead">
                <img class="modlogo" src="img/logotip.png" alt="">
            </div>
            <nav class="navhead">
                <!-- <a class="navlink activehead" href="#">Главная</a> -->
                <a class="navlink scrollToDown " href="#usl">Услуги</a>
                <a class="navlink scrollToDown" id="www99999" href="#part">Процесс создания</a>
                <!--                                <a class="navlink scrollToDown" id="www98888" href="#akc"></a>-->
                <a class="navlink scrollToDown" href="videos.php" target="_blank">Видео</a>
                <a class="navlink scrollToDown" href="all.php" target="_blank">Галерея</a>
                <!--                <a class="navlink scrollToDown" href="meropr.php">Мероприятия</a>-->
                <a class="navlink scrollToDown activelink" href="#contact">Контакты</a>
                <!-- <img class="arrowhead" src="" alt=""> -->
            </nav>
        </div>
    </div>
</header>

<div class="introf">
    <div class="for-siema">
        <div class="siema">
            <div><img class="siema-img" src="./img/top-slider/1.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/2.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/3.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/4.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/5.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/6.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/7.jpg" alt="Siema image"/></div>
            <div><img class="siema-img" src="./img/top-slider/8.jpg" alt="Siema image"/></div>
        </div>
        <script src="./js/siema.min.js" charset="utf-8"></script>
    </div>
    <div class="intro">
        <div class="container">
            <div class="titleinhead">
                <script type="text/javascript">
                    $(function () {


                        setTimeout(function () {
                            setTimeout(function () {
                                setTimeout(function () {


                                    setTimeout(function () {
                                        setTimeout(function () {
                                            setTimeout(function () {
                                                setTimeout(function () {
                                                    setTimeout(function () {
                                                        setTimeout(function () {
                                                            setTimeout(function () {
                                                                setTimeout(function () {
                                                                    setTimeout(function () {
                                                                        setTimeout(function () {
                                                                            setTimeout(function () {
                                                                                setTimeout(function () {
                                                                                    setTimeout(function () {
                                                                                        setTimeout(function () {
                                                                                            setTimeout(function () {

                                                                                                $('.introtitle').hide(500);
                                                                                                $('.introtitle').empty();
                                                                                                $('.introtitle').append("<span class='avtodel'>мероприятие в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                                                                            }, 4000);
                                                                                            $('.introtitle').hide(500);
                                                                                            $('.introtitle').empty();
                                                                                            $('.introtitle').append("<span class='avtodel'> эксклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                                                                                        }, 4000);
                                                                                        $('.introtitle').hide(500);
                                                                                        $('.introtitle').empty();
                                                                                        $('.introtitle').append("<span class='avtodel'>индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                                                                                        // $('.introtitle').hide(500);
                                                                                    }, 4000);
                                                                                    $('.introtitle').hide(500);
                                                                                    $('.introtitle').empty();
                                                                                    $('.introtitle').append("<span class='avtodel'>мероприятие в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                                                                }, 4000);
                                                                                $('.introtitle').hide(500);
                                                                                $('.introtitle').empty();
                                                                                $('.introtitle').append("<span class='avtodel'> эксклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                                                                            }, 4000);
                                                                            $('.introtitle').hide(500);
                                                                            $('.introtitle').empty();
                                                                            $('.introtitle').append("<span class='avtodel'>Индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                                                                            // $('.introtitle').hide(500);
                                                                        }, 4000);
                                                                        $('.introtitle').hide(500);
                                                                        $('.introtitle').empty();
                                                                        $('.introtitle').append("<span class='avtodel'>Мероприятия в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                                                    }, 4000);
                                                                    $('.introtitle').hide(500);
                                                                    $('.introtitle').empty();
                                                                    $('.introtitle').append("<span class='avtodel'> Эсклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                                                                }, 4000);
                                                                $('.introtitle').hide(500);
                                                                $('.introtitle').empty();
                                                                $('.introtitle').append("<span class='avtodel'>Индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                                                                // $('.introtitle').hide(500);
                                                            }, 4000);
                                                            $('.introtitle').hide(500);
                                                            $('.introtitle').empty();
                                                            $('.introtitle').append("<span class='avtodel'>Мероприятия в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                                        }, 4000);
                                                        $('.introtitle').hide(500);
                                                        $('.introtitle').empty();
                                                        $('.introtitle').append("<span class='avtodel'> Эксклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                                                    }, 4000);
                                                    $('.introtitle').hide(500);
                                                    $('.introtitle').empty();
                                                    $('.introtitle').append("<span class='avtodel'>Индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                                                    // $('.introtitle').hide(500);
                                                }, 4000);
                                                $('.introtitle').hide(500);
                                                $('.introtitle').empty();
                                                $('.introtitle').append("<span class='avtodel'>Мероприятия в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                            }, 4000);
                                            $('.introtitle').hide(500);
                                            $('.introtitle').empty();
                                            $('.introtitle').append("<span class='avtodel'> Эксклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                                        }, 4000);
                                        $('.introtitle').hide(500);
                                        $('.introtitle').empty();
                                        $('.introtitle').append("<span class='avtodel'>Индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                                        // $('.introtitle').hide(500);
                                    }, 4000);


                                    $('.introtitle').hide(500);
                                    $('.introtitle').empty();
                                    $('.introtitle').append("<span class='avtodel'>Мероприятия в любой <span class='textgrad'>точке мира</span></span></h2>").show(500);
                                }, 4000);
                                $('.introtitle').hide(500);
                                $('.introtitle').empty();
                                $('.introtitle').append("<span class='avtodel'> Эксклюзивные  <span class='textgrad'>решения</span></span></h2>").show(500);
                            }, 4000);
                            $('.introtitle').hide(500);
                            $('.introtitle').empty();
                            $('.introtitle').append("<span class='avtodel'>Индивидуальный  <span class='textgrad'>подход</span></span></h2>").show(500);
                            // $('.introtitle').hide(500);
                        }, 4000);


                    });
                </script>

                <h2 id="uuuPARA" class='introtitle'></h2>
                <h4 class="introtitlebott">индивидуальные решения</h4>
                <a href="#nextes" class="scrollToDown button">
                    <svg class="button-svg">
                        <rect class="button-rect"></rect>
                    </svg>
                    Далее
                </a>
            </div>
        </div>
    </div>
</div>

<section class="layer" id="nextes">
    <div class="container">
        <div class="titlemid">
            <h3 class="sectionsubtitle">Преимущества</h3>
            <h2 class="sectiontitle">Почему выбирают нас?</h2>
        </div>

        <div class="prime">
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/int.png" alt="">
                    <div class="primetitle">Мы чувствуем Ваше настроение</div>
                    <div class="primetext">Мы моментально начинаем работать над вашим праздником, делаем его по
                        настоящему уникальным и весёлым. За счёт скорости работы мы можем осуществить самые разные
                        желания клиентов.
                    </div>
                </div>
            </div>
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/kint.png" alt="">
                    <div class="primetitle">Мы предлагаем уникальные решения Вашего праздника</div>
                    <div class="primetext">Мы подходим к каждому клиенту с индивидуальной точки зрения, находим
                        уникальные решения. Мы имеем возможность создать что угодно для вашего торжества.
                    </div>
                </div>
            </div>
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/dint.png" alt="">
                    <div class="primetitle">Вы отдыхаете, мы занимаемся организацией</div>
                    <div class="primetext">Мы можем сделать что-то по истине уникальное и захватывающее для каждого
                        клиента. Ведь для каждого свой подход.
                    </div>
                </div>
            </div>
        </div>
        <div class="prime">
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/gint.png" alt="">
                    <div class="primetitle">Всё сделаем мы</div>
                    <div class="primetext">Воплотим в жизнь все Ваши идеи, даже если они кажутся Вам фантастическими.
                    </div>
                </div>
            </div>
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/pint.png" alt="">
                    <div class="primetitle">В любом формате</div>
                    <div class="primetext">С нами любое место проведения доступно для Вас.</div>
                </div>
            </div>
            <div class="hoverinprime">
                <div class="primeitem">
                    <img class="primeimg" src="img/services/tint.png" alt="">
                    <div class="primetitle">В любом месте</div>
                    <div class="primetext">Мы создаём мероприятие под ключ и предоставляем абсолютно все услуги в
                        организации Вашего события.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="videoscreen" id="video">
    <div class="titlemid">
        <h3 class="sectionsubtitle"></h3>
        <h2 class="sectiontitle">О наших праздниках</h2>
    </div>
    <div class="video">
        <?php
        $dir = opendir("./video/");
        $count = 0;

        $allv = 0;
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..' || is_dir('./video' . $file)) {
                continue;
            }
            $count++;
        }

        if ($allv == 1) {
            for ($i = 1; $i < $count; $i++) {
                echo file_get_contents('./video/' . $i . '.txt');
            }
        } else {
            for ($i = 1; $i < 5; $i++) {
                echo file_get_contents('./video/' . $i . '.txt');
            }
        }


        ?>
    </div>
    <center>
        <a href="videos.php"><img src="./img/top-img.png" alt="увидеть всё"
                                  style="transform: rotate(180deg);margin: 1em;"></a>
    </center>
</section>

<div class="statistics">
    <div class="container">
        <?php
        $stats_1 = './img/statistick/1.txt';
        $stats_2 = './img/statistick/2.txt';
        $stats_3 = './img/statistick/3.txt';
        $stats_4 = './img/statistick/4.txt';
        $stats_5 = './img/statistick/5.txt';
        ?>
        <div class="statitems">
            <div class="stitem">
                <div class="stnum"><?php echo file_get_contents($stats_1); ?></div>
                <div class="sttext">крутых розыграшей</div>
            </div>
            <div class="stitem">
                <div class="stnum"><?php echo file_get_contents($stats_2); ?></div>
                <div class="sttext">корпоративных мероприятий</div>
            </div>
            <div class="stitem">
                <div class="stnum"><?php echo file_get_contents($stats_3); ?></div>
                <div class="sttext">довольных клиентов</div>
            </div>
            <div class="stitem">
                <div class="stnum"><?php echo file_get_contents($stats_4); ?></div>
                <div class="sttext">детских праздников</div>
            </div>
            <div class="stitem">
                <div class="stnum"><?php echo file_get_contents($stats_5); ?></div>
                <div class="sttext">лет радости</div>
            </div>
        </div>
    </div>
</div>
<section class="layer" id="part">
    <div class="container">

        <div class="titlemid">
            <h3 class="sectionsubtitle"></h3>
            <h2 class="sectiontitle">Как мы создаём праздник</h2>
            <div class="sectiontext">
                <p>
                    Вы отдыхаете, работаем только мы
                </p>
            </div>
        </div>

        <div class="portfolio">
            <div class="portfolitem">
                <div class="trans">
                    <img class="porfolioimg" src="img/services/portfolioflowers.jpg" alt="">
                </div>
            </div>
            <div class="portfolitem" id="accordion">

                <h3 class="statshead">
                    <div class="topper">
                        <img src="img/services/tintsmall.png" alt="" class="statsimg">
                        <div class="statstitle">Персональные пожелания</div>
                    </div>
                </h3>
                <div class="content">
                    <p>Мы учитываем все Ваши пожелания и воплотим в жизнь любые идеи</p>
                </div>

                <h3 class="statshead">
                    <div class="topper">
                        <img src="img/services/perintsmall.png" alt="" class="statsimg">
                        <div class="statstitle">Уникальные решения</div>
                    </div>
                </h3>
                <div class="content">
                    <p>Мы предлагаем Вам эксклюзивные решения Вашего события</p>
                </div>

                <h3 class="statshead">
                    <div class="topper">
                        <img src="img/services/uniksmall.png" alt="" class="statsimg">
                        <div class="statstitle">Весь спектр услуг</div>
                    </div>
                </h3>
                <div class="content">
                    <p>Мы обеспечиваем весь спектр услуг в организации Вашего мероприятия</p>
                </div>

                <h3 class="statshead">
                    <div class="topper">
                        <img src="img/services/tintsmall.png" alt="" class="statsimg">
                        <div class="statstitle">Высокий уровень выполнения работы</div>
                    </div>
                </h3>
                <div class="content">
                    <p>На протяжении всего сотрудничества с нами , мы на связи 24/7 и готовы проконсультировать по
                        любому вопросу</p>
                </div>

            </div><!--portfolioitem-->
        </div><!--portfolio-->

    </div>
</section>


<section class="layer">
    <div class="container">
        <div class="titlemid">
            <h3 class="sectionsubtitle">The Best</h3>
            <h2 class="sectiontitle">Наш инстаграм</h2>
            <div class="sectiontext">
                <br>
                <p></p>
            </div>
        </div>
        <div class="services offers">

            <div class="servicesfoto">
                <div class="social">
                    <a class="socialitem" href="https://www.instagram.com/best_party_event/" target="_blank">
<!--                        <img class="fa-instagram" src="img/socials/inst.png" alt="">-->
                        <svg class="fa-instagram" width="100%" height="100%" viewBox="0 0 200 200">
                            <defs>
                                <!-- 矩形的線性漸層 -->
                                <linearGradient id="gradient1" x1=".8" y1=".8" x2="0">
                                    <stop offset="0" stop-color="#c92bb7"/>
                                    <stop offset="1" stop-color="#3051f1"/>
                                </linearGradient>
                                <!-- 矩形的放射漸層 -->
                                <radialGradient id="gradient2" cx=".2" cy="1" r="1.2">
                                    <stop offset="0" stop-color="#fcdf8f"/>
                                    <stop offset=".1" stop-color="#fbd377"/>
                                    <stop offset=".25" stop-color="#fa8e37"/>
                                    <stop offset=".35" stop-color="#f73344"/>
                                    <stop offset=".65" stop-color="#f73344" stop-opacity="0"/>
                                </radialGradient>
                                <!-- 矩形外框 -->
                                <rect id="logoContainer" x="0" y="0" width="200" height="200" rx="50" ry="50"/>
                            </defs>

                            <!-- colorful 的背景 -->
                            <use xlink:href="#logoContainer" fill="url(#gradient1)"/>
                            <use xlink:href="#logoContainer" fill="url(#gradient2)"/>

                            <!-- 相機外框 -->
                            <rect x="35" y="35" width="130" height="130" rx="30" ry="30"
                                  fill="none" stroke="#fff" stroke-width="13"/>

                            <!-- 鏡頭外框 -->
                            <circle cx="100" cy="100" r="32"
                                    fill="none" stroke="#fff" stroke-width="13"/>

                            <!-- 閃光燈 -->
                            <circle cx="140" cy="62" r="9" fill="#fff"/>
                        </svg>
                        <p>best_party_event</p>
                    </a>
                    <a class="socialitem scit2" href="https://www.instagram.com/best_party_decor/" target="_blank">
<!--                        <img class="fa-instagram" src="img/socials/inst.png" alt="">-->
                        <svg class="fa-instagram" width="100%" height="100%" viewBox="0 0 200 200">
                            <defs>
                                <!-- 矩形的線性漸層 -->
                                <linearGradient id="gradient1" x1=".8" y1=".8" x2="0">
                                    <stop offset="0" stop-color="#c92bb7"/>
                                    <stop offset="1" stop-color="#3051f1"/>
                                </linearGradient>
                                <!-- 矩形的放射漸層 -->
                                <radialGradient id="gradient2" cx=".2" cy="1" r="1.2">
                                    <stop offset="0" stop-color="#fcdf8f"/>
                                    <stop offset=".1" stop-color="#fbd377"/>
                                    <stop offset=".25" stop-color="#fa8e37"/>
                                    <stop offset=".35" stop-color="#f73344"/>
                                    <stop offset=".65" stop-color="#f73344" stop-opacity="0"/>
                                </radialGradient>
                                <!-- 矩形外框 -->
                                <rect id="logoContainer" x="0" y="0" width="200" height="200" rx="50" ry="50"/>
                            </defs>

                            <!-- colorful 的背景 -->
                            <use xlink:href="#logoContainer" fill="url(#gradient1)"/>
                            <use xlink:href="#logoContainer" fill="url(#gradient2)"/>

                            <!-- 相機外框 -->
                            <rect x="35" y="35" width="130" height="130" rx="30" ry="30"
                                  fill="none" stroke="#fff" stroke-width="13"/>

                            <!-- 鏡頭外框 -->
                            <circle cx="100" cy="100" r="32"
                                    fill="none" stroke="#fff" stroke-width="13"/>

                            <!-- 閃光燈 -->
                            <circle cx="140" cy="62" r="9" fill="#fff"/>
                        </svg>
                        <p>best_party_decor</p>
                    </a>
                </div>
            </div>

        </div>

    </div>

    </div>
</section>


<section class="layer" id="akc">
    <div class="container">

        <div class="titlemid">
            <h3 class="sectionsubtitle">Яркие моменты</h3>
            <h2 class="sectiontitle">Моменты с наших проектов</h2>
            <div class="sectiontext">
                <p>
                    Тут вы сможете увидеть счастливые лица некоторых, кто когда либо праздновал у нас. Вы можете увидеть
                    интересную тематику, в которой вам захочеться провести ваш личный праздник.
                </p>
            </div>
        </div>
    </div>
    <div class="works">
        <div class="workscolum">
            <div class="worksitem">
                <img class="workimg" src="img/works/1.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_1 = './img/works/1.txt';
                    $works_date_1 = './img/works/1date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_1); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_1); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/2.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_2 = './img/works/2.txt';
                    $works_date_2 = './img/works/2date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_2); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_2); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/3.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_3 = './img/works/3.txt';
                    $works_date_3 = './img/works/3date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_3); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_3); ?></div>
                </div>
            </div>

        </div>
        <div class="workscolum">
            <div class="worksitem">
                <img class="workimg" src="img/works/4.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_4 = './img/works/4.txt';
                    $works_date_4 = './img/works/4date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_4); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_4); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/5.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_5 = './img/works/5.txt';
                    $works_date_5 = './img/works/5date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_5); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_5); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/6.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_6 = './img/works/6.txt';
                    $works_date_6 = './img/works/6date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_6); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_6); ?></div>
                </div>
            </div>
        </div>
        <div class="workscolum">
            <div class="worksitem" id="workimg">
                <img class="workimg" src="img/works/7.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_7 = './img/works/7.txt';
                    $works_date_7 = './img/works/7date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_7); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_7); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/8.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_8 = './img/works/8.txt';
                    $works_date_8 = './img/works/8date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_8); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_8); ?></div>
                </div>
            </div>
        </div>
        <div class="workscolum">
            <div class="worksitem">
                <img class="workimg" src="img/works/9.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_9 = './img/works/9.txt';
                    $works_date_9 = './img/works/9date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_9); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_9); ?></div>
                </div>


            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/10.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_10 = './img/works/10.txt';
                    $works_date_10 = './img/works/10date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_10); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_10); ?></div>
                </div>
            </div>
            <div class="worksitem">
                <img class="workimg" src="img/works/11.jpg" alt="">
                <div class="workshover">
                    <img class="workimglogo" src="img/works/gallery.png" alt="">
                    <?php
                    $works_img_11 = './img/works/11.txt';
                    $works_date_11 = './img/works/11date.txt';
                    ?>
                    <div class="worktitle"><?php echo file_get_contents($works_img_11); ?></div>
                    <div class="worksubtitle"><?php echo file_get_contents($works_date_11); ?></div>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="layer" id="usl">
    <div class="container">
        <div class="sales">

            <div class="titlemid">
                <h3 class="sectionsubtitle">Хотите что-то отпраздновать?</h3>
                <h2 class="sectiontitle">Наши услуги</h2>
                <div class="sectiontext">
                    <p>
                        Все на Ваш выбор. Нажимайте, не стесняйтесь! Посмотрите наши тематики, мы уверены, что Вы
                        увидите что-то захватывающее и интересное.
                    </p>
                </div>
            </div>
            <div class="sale">
                <div class="saleitems scalesale">
                    <a href="dr.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/1.jpg" alt="" class="saleimg">
                            <div class="datein">
                                <div class="datesnum">-15%</div>
                                <div class="datesmonth">Sale</div>
                            </div>
                        </div>
                        <div class="saletitle">Дни рождения</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                                День Рождения - это особый праздник для каждого, мы, как никто, это понимаем.
                            </div>
                            <div class="saletextbottom">
                                Организация мероприятия любой сложности, в любой точке мира, только Ваше желание и наш
                                профессионализм, и этот день останется в Вашем сердце навсегда.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>
                    </a>
                </div>
                <div class="saleitems scalesale">
                    <a href="korpor.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/2.jpg" alt="" class="saleimg">
                        </div>
                        <div class="saletitle">Корпоративные мероприятия</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                                Мы знаем, что такое рабочие будни, и как важен отдых для Вас.
                            </div>
                            <div class="saletextbottom">
                                Мы разработаем концепцию корпоративного мероприятия, учитывая все пожелания Вашего
                                коллектива и каждого сотрудника отдельно. Подберем локацию, обеспечим все необходимые
                                условия и создадим уникальный проект.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>
                    </a>
                </div>
                <div class="saleitems scalesale">
                    <a href="swad.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/3.jpg" alt="" class="saleimg">
                            <div class="datein">
                                <div class="datesnum">-5%</div>
                                <div class="datesmonth">Sale</div>
                            </div>
                        </div>
                        <div class="saletitle">Свадебные церемонии</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                                Самый важный день в Вашей жизни должен быть самым счастливым, светлым и лёгким, и мы
                                точно знаем как это осуществить!
                            </div>
                            <div class="saletextbottom">
                                Доверьте нам все Ваши хлопоты, переживания и волнения. В этот день позвольте себе
                                наслаждаться друг другом, а мероприятием займутся профессионалы.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>
                    </a>
                </div>

            </div>

            <div class="sale">
                <div class="saleitems scalesale">
                    <a href="kidsdr.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/4.jpg" alt="" class="saleimg">
                            <div class="datein">
                                <div class="datesnum">-30%</div>
                                <div class="datesmonth">Sale</div>
                            </div>
                        </div>
                        <div class="saletitle">Детские дни рождения</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                            </div>
                            <div class="saletextbottom">
                                Мы раскроем все таланты Вашего чада и подберем самую интересную программу для праздника.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>
                    </a>
                </div>
                <div class="saleitems scalesale">
                    <a href="party.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/5.jpg" alt="" class="saleimg">

                        </div>
                        <div class="saletitle">Розыгрыши и сюрпризы</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                            </div>
                            <div class="saletextbottom">
                                Удивить именинника или поздравить любимого человека, организовать целый день сюрпризов
                                или придумать сценарий для яркого розыгрыша - решать Вам.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>
                    </a>
                </div>
                <div class="saleitems scalesale">
                    <a href="presents.php" target="_blank">
                        <div class="salehead">
                            <img src="img/sale/6.jpg" alt="" class="saleimg">

                        </div>
                        <div class="saletitle">Презентации и конференции</div>
                        <div class="saletexts">
                            <div class="saletexttop">
                            </div>
                            <div class="saletextbottom">
                                Подбор локации, организация кейтеринга и наш творческий подход помогут провести
                                презентацию либо конференцию эффективно.
                            </div>
                        </div>
                        <a class="scrollToDown" href="#contact">
                            <div class="salenum">Набрать!</div>
                        </a>
                        <div class="saleinfo">Звоните Сейчас</div>

                    </a>
                </div>

            </div>
        </div>
    </div>
</section>


<section id="contact" class="layer wallper number-hover">
    <div class="container">
        <div class="numbers">
            <div class="numberite"><a href="tel:+380675171607"><span class="modcolorcontact">067</span> 517 1607</a>
            </div>
            <div class="numberite"><a href="tel:+380675650045"><span class="modcolorcontact">067</span> 565 0045</a>
            </div>

        </div>

        <div class="animnames">
            <div class="namescontact">
                <div class="animmname">
                    <div class="anmconitem">Наталья</div>
                    <div class="anmconitem anmconmod">Олег</div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="boxxer">
    <div class="arround">

        <div class="salehead saleitemsXS">

        </div>

        <div class="saleitemsXS qwertysso">
            <div class="texst">
                <div class="saletitle">Оформление и декор</div>
                <div class="saletexts">
                    <div class="saletexttop">
                        Оформление мероприятий любой сложности. Наше Агенство предлагает создание декораций, их аренду,
                        оформление а также многое другое.
                    </div>
                    <div class="saletextbottom">
                        Мы поможем Вам воплотить в жизнь любую идею!
                    </div>
                </div>
                <a class="scrollToDown" href="#contact">
                    <div class="salenum">Звоните Сейчас</div>
                </a>
            </div>

        </div>
        <div class="salehead saleitemsXS">


        </div>
        <div class="saleitemsXS qwertysso">

            <div class="texst">
                <div class="saletitle">Ростовые куклы и аренда костюмов</div>
                <div class="saletexts">
                    <div class="saletexttop">
                        Хотите эффектно поздравить любимого человека? Или устроить вечеринку в тематических костюмах?
                        Или подарить яркий розыгрыш другу?
                    </div>
                    <div class="saletextbottom">
                        Мы поможем Вам осуществить любую идею! Наши ростовые куклы подарят улыбки и поднимут настроение!
                    </div>
                </div>
                <a class="scrollToDown" href="#contact">
                    <div class="salenum">Звоните Сейчас</div>
                </a>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="containerX">
        <div class="footerin">
            <div class="saleitemsX">
                <iframe class="framemapa"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1323.1450181966743!2d35.06132665823252!3d48.450964394812466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbe2d5998b1bb5%3A0x94a05d15c7f1f9c7!2z0YPQuy4g0JDQutCw0LTQtdC80LjQutCwINCn0LXQutC80LDRgNC10LLQsCwgMiwg0JTQvdC40L_RgNC-LCDQlNC90LXQv9GA0L7Qv9C10YLRgNC-0LLRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNDkwMDA!5e0!3m2!1sru!2sua!4v1608926678412!5m2!1sru!2sua"
                        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                        aria-hidden="false" tabindex="0"></iframe>

                <div class="footercol footmod">
                    <div class="foottitle">Best Party</div>
                    <div class="footext">
                        Вместе мы создаем праздник, частью которого хочется стать!<br/>
                        Эмоции которые невозможно забыть!<br/>
                        Опыт, которым хочется поделиться!<br/>
                        Мероприятия, на которых действительно стоит побывать!
                        <br><br> У вас возникли вопросы или надо организовать мероприятие – мы всегда на связи и рады
                        общению.
                    </div>
                    <!-- <div class="footext">
                        Вместе мы создаем событие, частью которого хочется стать, эмоции, которые невозможно забыть, опыт, которым хочется поделиться, мероприятия , на которых действительно стоит побывать. <br><br> У вас возникли вопросы или надо организовать мероприятие – мы всегда на связи и рады  общению.
                    </div> -->
                    <div class="numbersfoot">
                        <div class="numitems"><a href="mailto:Bestpartyevent@gmail.com">Bestpartyevent@gmail.com</a>
                        </div>
                        <div class="numitems"><a href="mailto:info@bestparty.dp.ua">info@bestparty.dp.ua</a></div>
                    </div>
                    <div class="numbersfoot" style="margin-top: -10%;">
                        <div class="numitems"><a href="https://www.instagram.com/best_party_event/">
                                <svg id="svg" width="100%" height="100%" viewBox="0 0 200 200">
                                    <defs>
                                        <!-- 矩形的線性漸層 -->
                                        <linearGradient id="gradient1" x1=".8" y1=".8" x2="0">
                                            <stop offset="0" stop-color="#c92bb7"/>
                                            <stop offset="1" stop-color="#3051f1"/>
                                        </linearGradient>
                                        <!-- 矩形的放射漸層 -->
                                        <radialGradient id="gradient2" cx=".2" cy="1" r="1.2">
                                            <stop offset="0" stop-color="#fcdf8f"/>
                                            <stop offset=".1" stop-color="#fbd377"/>
                                            <stop offset=".25" stop-color="#fa8e37"/>
                                            <stop offset=".35" stop-color="#f73344"/>
                                            <stop offset=".65" stop-color="#f73344" stop-opacity="0"/>
                                        </radialGradient>
                                        <!-- 矩形外框 -->
                                        <rect id="logoContainer" x="0" y="0" width="200" height="200" rx="50" ry="50"/>
                                    </defs>

                                    <!-- colorful 的背景 -->
                                    <use xlink:href="#logoContainer" fill="url(#gradient1)"/>
                                    <use xlink:href="#logoContainer" fill="url(#gradient2)"/>

                                    <!-- 相機外框 -->
                                    <rect x="35" y="35" width="130" height="130" rx="30" ry="30"
                                          fill="none" stroke="#fff" stroke-width="13"/>

                                    <!-- 鏡頭外框 -->
                                    <circle cx="100" cy="100" r="32"
                                            fill="none" stroke="#fff" stroke-width="13"/>

                                    <!-- 閃光燈 -->
                                    <circle cx="140" cy="62" r="9" fill="#fff"/>
                                </svg>
                                best_party_event</a>
                        </div>
                        <div class="numitems" style=""><a href="https://www.instagram.com/best_party_decor/">
                                <svg id="svg" width="100%" height="100%" viewBox="0 0 200 200">
                                    <defs>
                                        <!-- 矩形的線性漸層 -->
                                        <linearGradient id="gradient1" x1=".8" y1=".8" x2="0">
                                            <stop offset="0" stop-color="#c92bb7"/>
                                            <stop offset="1" stop-color="#3051f1"/>
                                        </linearGradient>
                                        <!-- 矩形的放射漸層 -->
                                        <radialGradient id="gradient2" cx=".2" cy="1" r="1.2">
                                            <stop offset="0" stop-color="#fcdf8f"/>
                                            <stop offset=".1" stop-color="#fbd377"/>
                                            <stop offset=".25" stop-color="#fa8e37"/>
                                            <stop offset=".35" stop-color="#f73344"/>
                                            <stop offset=".65" stop-color="#f73344" stop-opacity="0"/>
                                        </radialGradient>
                                        <!-- 矩形外框 -->
                                        <rect id="logoContainer" x="0" y="0" width="200" height="200" rx="50" ry="50"/>
                                    </defs>

                                    <!-- colorful 的背景 -->
                                    <use xlink:href="#logoContainer" fill="url(#gradient1)"/>
                                    <use xlink:href="#logoContainer" fill="url(#gradient2)"/>

                                    <!-- 相機外框 -->
                                    <rect x="35" y="35" width="130" height="130" rx="30" ry="30"
                                          fill="none" stroke="#fff" stroke-width="13"/>

                                    <!-- 鏡頭外框 -->
                                    <circle cx="100" cy="100" r="32"
                                            fill="none" stroke="#fff" stroke-width="13"/>

                                    <!-- 閃光燈 -->
                                    <circle cx="140" cy="62" r="9" fill="#fff"/>
                                </svg>
                                best_party_decor</a>
                        </div>
                    </div>
                    <div class="numbersfoot" style="
    margin-top: -10%;
    border-bottom: 0;
">
                        <div class="numitems"><a href="tel:+380675650045">+380 067 565 0045</a>

                        </div>
                        <div class="numitems" style="
"><a href="tel:+380675171607">+380 067 517 1607</a></div>
                    </div>
                </div>
            </div>
            <!-- <div class="footercol footmodsec">
                <div class="footername">недавнее</div>
                <div class="second">
                    <div class="seconditem">
                        <img class="secondimage" src="" alt="">
                        <div class="sectexts">
                            <p class="seca"href="#">Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril.</p>
                            <div class="secadate">15 Мая 2019</div>
                        </div>
                    </div>
                </div>
                <div class="second">
                    <div class="seconditem">
                        <img class="secondimage" src="img/works/2.png" alt="">
                        <div class="sectexts">
                            <p class="seca"href="#">Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril.</p>
                            <div class="secadate">15 Мая 2019</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footercol footmodsec">
                <div class="footername">События</div>
                <div class="second">
                    <div class="seconditem">
                        <img class="secondimage" src="img/works/1.png" alt="">
                        <div class="sectexts">
                            <p class="seca"href="#">Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril </p>
                            <div class="secadate">15 Мая 2019</div>
                        </div>
                    </div>
                </div>
                <div class="second">
                    <div class="seconditem">
                        <img class="secondimage" src="img/works/2.png" alt="">
                        <div class="sectexts">
                            <p class="seca"href="#">Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril </p>
                            <div class="secadate">15 Мая 2019</div>
                        </div>
                    </div>
                </div>
                <div class="second">
                    <div class="seconditem">
                        <img class="secondimage" src="img/works/5.png" alt="">
                        <div class="sectexts">
                            <p class="seca"href="#">Lorem ipsum dolor sit amet, et utroque laoreet neglegentur eam, no eam zril </p>
                            <div class="secadate">15 Мая 2019</div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>

</footer>
<div class="footerbott">
    <div class="foottitleb">Каждый пиксель защищён.</div>
    <div class="footsubtitle">&copy; 2013-2021 Создано с радостью. Bond&PaChi</div>
    <div class="sp-footer">
        <a href="https://t.me/defproger"><span>Made by </span></a>
        <div class="sirius">
            <a href="https://t.me/defproger"><img class="create" src="img/favicon/creators.png" alt=""></a>
        </div>
    </div>


</div>
<a href="#" class="to-top">Наверх</a>
</body>
</html>
